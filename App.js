import React, { Component } from 'react'
import Register from './src/pages/Register';
import Login from './src/pages/Login';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import { Provider } from 'react-redux';
import configureStore from './store';
import Home from './src/pages/Home';
import Profile from './src/pages/Profile';
const store = configureStore();

class HomeScreen extends Component {
  render() {
    return <Login />
  }
}

class Registers extends Component {
  render() {
    return <Register />
  }
}

class Homes extends Component {
  render() {
    return <Home />
  }
}

class Profiles extends Component {
  render() {
    return <Profile />
  }
}

const StackNav = createStackNavigator({
  Logins: {
    screen: HomeScreen
  },
  Register: {
    screen: Registers
  },
  Home:{
    screen: Homes
  },
  Profile:{
    screen: Profiles
  }
},
  {
    initialRouteName: 'Logins',
    headerMode: 'none'
  })

const AppContainer = createAppContainer(StackNav)

export default class App extends Component {
  render() {
    return(
      <Provider store={store}>
        <AppContainer />
      </Provider>
    )
  }
}
