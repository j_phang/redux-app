import axios from 'axios';

export const apiGetUser = token => {
    console.log('token-nya ', token);
    return axios.get(
        `http://basic-todoapi.herokuapp.com/api/user/show/`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `jwt ${token}`
            }
        }
    )
        .then(result => {
            console.log(result.data)
            return result.data
        })
        .catch(e => {
            console.log(e)
        })

};
