import React, { Component } from 'react'
import { Text, ToastAndroid, ImageBackground, Dimensions } from 'react-native'
import { withNavigation } from 'react-navigation';
import { Item, Button, Container, View, Label, Input, Header, Left, Icon, Body, Title, Toast, Picker, Right } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import Axios from 'axios';
import styles from './style';
import { connect } from 'react-redux'
import { auth } from '../redux/action/AuthAction'

const mapStateToProps = state => ({
    auth: state.auth
})

class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            email: '',
            password: ''
        }
    }
    async daftar() {
        try {
            const postAkun = async (objParam) => await Axios.post(
                `http://basic-todoapi.herokuapp.com/api/user/create`, objParam, {
                    headers: {
                        'Content-Type': 'application/json'
                    }
                }
            )
            // if (this.state.password.length >= 8) {
            postAkun({
                username: this.state.username,
                email: this.state.email,
                password: this.state.password
            })
                .then(response => {
                    console.log(response)
                    ToastAndroid.show('Anda Berhasil Daftar!', ToastAndroid.SHORT)
                    this.setState({
                        username: '',
                        email: '',
                        password: ''
                    })
                    ToastAndroid.show(this.state.gender, ToastAndroid.SHORT)
                    this.props.navigation.pop()
                })
                .catch(e => {
                    console.log(e.response.data)
                    ToastAndroid.show('GAGAL WKWKWWKWK', ToastAndroid.SHORT)
                })
            // } else {
            //     ToastAndroid.show('Password Minimal 8 Karakter Sayang', ToastAndroid.SHORT)
            // }
        }
        catch (e) {
            console.log(e)
            ToastAndroid.show('Gatau salah apa ini', ToastAndroid.SHORT)
        }
    }
    render() {
        return (
            <Container>
                <ImageBackground
                    source={require('../bgd/Register.jpg')}
                    style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height }}
                >
                    <Header transparent>
                        <Left>
                            <Button transparent onPress={() => this.props.navigation.pop()}>
                                <Icon name='arrow-back' />
                            </Button>
                        </Left>
                        <Body>
                            <Title>Registration Page</Title>
                        </Body>
                    </Header>
                    <ScrollView>
                        <View
                            style={{
                                display: 'flex',
                                justifyContent: 'center',
                                alignContent: 'center',
                                marginTop: '80%'
                            }}
                        >
                            <Item floatingLabel style={styles.input}>
                                <Label>
                                    <Text style={styles.text}>Username</Text>
                                </Label>
                                <Input
                                    style={{ color: 'white' }}
                                    keyboardType='default'
                                    value={this.state.username}
                                    onChangeText={(text) => { this.setState({ username: text }) }}
                                />
                            </Item>
                            {/* <Picker
                            selectedValue={this.state.gender}
                            style={{ height: 50, width: '100%' }}
                            onValueChange={
                                (itemValue) => {
                                    this.setState({ gender: itemValue })
                                }

                            }
                        >
                            <Picker.Item label="Male" value="Male" />
                            <Picker.Item label="Female" value="Female" />
                        </Picker> */}
                            <Item floatingLabel style={styles.input}>
                                <Label>
                                    <Text style={styles.text}>Email</Text>
                                </Label>
                                <Input
                                    style={{ color: 'white' }}
                                    keyboardType='email-address'
                                    secureTextEntry={true}
                                    value={this.state.email}
                                    onChangeText={(text) => { this.setState({ email: text }) }}
                                />
                            </Item>
                            <Item floatingLabel style={styles.input}>
                                <Label>
                                    <Text style={styles.text}>Password</Text>
                                </Label>
                                <Input
                                    style={{ color: 'white' }}
                                    secureTextEntry={true}
                                    keyboardType='default'
                                    value={this.state.password}
                                    onChangeText={(text) => { this.setState({ password: text }) }}
                                />
                            </Item>
                            <Item style={styles.button}>
                                <Button transparent style={styles.buttons} full onPress={() => this.daftar()}>
                                    <Text style={styles.text}>Daftar!</Text>
                                </Button>
                            </Item>
                        </View>
                    </ScrollView>
                </ImageBackground>
            </Container>
        )
    }
}
export default withNavigation(connect(mapStateToProps)(Register))