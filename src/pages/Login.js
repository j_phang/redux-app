import React, { Component } from 'react'
import { Text, View, ImageBackground, Dimensions, Alert } from 'react-native'
import { withNavigation } from 'react-navigation';
import { Container, Content, Label, Input, Header, Body, Title, Item, Button } from 'native-base';
import Axios from 'axios';
import * as authType from '../redux/type/AuthType'
import { connect } from 'react-redux'
import { auth } from '../redux/action/AuthAction'

const mapStateToProps = state => ({
    auth: state.auth
})

const mapDispatchToProps = dispatch => {
    return {
        FetchToken: token => dispatch(auth(token))
    }
}

class Login extends Component {

    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: ''
        }
    }

    async login() {
        try {
            const objParam = {
                username: this.state.username,
                password: this.state.password
            }

            const response = await Axios.post(
                `http://basic-todoapi.herokuapp.com/api/user/login`, objParam
            )

            console.log(`Bisa Log In Sayang :*`)
            this.props.FetchToken(response.data.result)
            console.log(this.props.auth)
            this.props.navigation.navigate('Home')
        }
        catch (e) {
            console.log(e)
        }
    }

    render() {
        return (
            <Container
                style={{
                    flex: 1
                }}
            >
                <ImageBackground
                    source={require('../bgd/Login.jpg')}
                    style={{
                        width: Dimensions.get('window').width,
                        height: Dimensions.get('window').height
                    }}
                >
                    <Header transparent>
                        <Title
                            style={{
                                flexDirection: 'row',
                                flexWrap: 'wrap',
                                justifyContent: 'center',
                                paddingTop: 15
                            }}
                        >
                            Login Page Sayang
                        </Title>
                    </Header>

                    <Content>
                        <Item
                            floatingLabel
                            style={{
                                alignSelf: 'center',
                                width: '60%'
                            }}
                        >
                            <Label>
                                <Text>Username Sayang</Text>
                            </Label>
                            <Input
                                value={this.state.username}
                                onChangeText={text => this.setState({
                                    username: text
                                })}
                            />
                        </Item>
                        <Item
                            floatingLabel
                            style={{
                                alignSelf: 'center',
                                width: '60%'
                            }}
                        >
                            <Label>
                                <Text>Password Sayang</Text>
                            </Label>
                            <Input
                                secureTextEntry={true}
                                value={this.state.password}
                                onChangeText={text => this.setState({
                                    password: text
                                })}
                            />
                        </Item>
                        <Button
                            style={{
                                alignSelf: 'center',
                                width: '60%'
                            }}
                            full
                            bordered
                            onPress={() => {
                                if (this.state.username == '' || this.state.password == '') {
                                    Alert.alert(
                                        'Username atau Passwordnya Masih Kosong',
                                        'Tolong Diisin Kolom Username dan Passwornya dong sayang',
                                        [
                                            { text: 'Ok', onPress: () => console.log('wow') }
                                        ]
                                    )
                                } else {
                                    this.login()
                                    this.setState({
                                        username: '',
                                        password: ''
                                    })
                                }
                            }}>
                            <Text>Login dong~</Text>
                        </Button>

                        <Button
                            style={{
                                alignSelf: 'center',
                                width: '60%'
                            }}
                            full
                            bordered
                            onPress={
                                () => this.props.navigation.navigate('Register')
                            }
                        >
                            <Text>Register</Text>
                        </Button>
                    </Content>
                </ImageBackground>
            </Container>
        )
    }
}

export default withNavigation(connect(mapStateToProps, mapDispatchToProps)(Login))
