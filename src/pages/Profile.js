import React, { Component } from 'react'
import { Text, View, ImageBackground, Dimensions, Image } from 'react-native'
import { withNavigation } from 'react-navigation';
import { connect } from 'react-redux'
import { getUser } from '../redux/action/UserAction';
import { Button, Container, Header, Title, Content, Left, Icon, Body } from 'native-base';

const mapStateToProps = state => ({
    auth: state.auth,
    user: state.user
})

class Profile extends Component {

    constructor(props) {
        super(props)
        this.state = {
            data: this.props.user.data.result
        }
    }

    componentDidMount() {
        console.log(this.props.user.data.result)
    }

    render() {
        return (
            <Container style={{
                flex: 1
            }}>
                <ImageBackground
                    source={require('../bgd/ProfileBg.jpg')}
                    style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height }}
                >
                    <Header transparent>
                        <Left
                            style={{
                            }}
                        >
                            <Button transparent onPress={() => this.props.navigation.pop()}>
                                <Icon name='arrow-back' />
                            </Button>
                        </Left>
                        <Body>
                            <Title
                                style={{
                                    flexDirection: 'row',
                                    flexWrap: 'wrap',
                                    justifyContent: 'center',
                                    color: 'white'
                                }}
                            >
                                Profile Lo Sayang
                            </Title>
                        </Body>
                    </Header>
                    <Content>
                        <Image
                            source={require('../bgd/Profile.jpg')}
                            style={{
                                width: 100,
                                height: 100,
                                borderRadius: 100 / 2,
                                alignSelf: 'center'
                            }}
                        />
                        <Text
                            style={{
                                color: 'white',
                                alignSelf: 'center',
                                fontSize: 34
                            }}
                        >
                            {this.state.data.email}
                            {/* {JSON.stringify(this.state.data)} */}
                        </Text>
                    </Content>
                </ImageBackground>
            </Container>
        )
    }
}

export default withNavigation(connect(mapStateToProps)(Profile))