import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
    header: {
      color: '#15183C'
    },
    inputField: {
      color: 'white',
      borderColor: 'white',
      marginTop: 5,
      width: '95%'
    },
    Wrapper: {
      display: 'flex',
      justifyContent: 'center',
      alignContent: 'center',
      width: '100%',
      height: '100%'
    },
    text: {
      color: 'white',
      fontSize: 20,
      display: "flex",
      alignSelf: 'center',
      justifyContent: "center",
      alignContent: "center"
    },
    texts: {
      color: 'white',
      fontSize: 25,
      alignSelf: 'center',
      display: "flex",
      justifyContent: 'center',
      alignContent: 'center'
    },
    input: {
      display: "flex",
      marginLeft: '20%',
      width: `60%`,
      justifyContent: 'center'
    },
    label: {
      width: 80
    },
    button: {
      display: "flex",
      borderColor: 'transparent',
      margin: 10,
      justifyContent: "center"
    },
    check: {
      margin: 20
    },
    container: {
      display: 'flex',
      justifyContent: 'center',
      alignContent: 'center'
    },
    buttons: {
        width: '60%', 
        
    },
    icons: {
        margin: '5%',
        color: 'white'
    },
    left: {
        marginLeft: '-60%'
    },
    right: {
        marginRight: '-65%',
        marginTop: '-45%'
    }
  })
export default styles